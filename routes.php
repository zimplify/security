<?php
    use Zimplify\Security\Controllers\ConnectionCreateController;
    use Zimplify\Security\Controllers\ConnectionReinstateController;
    use Zimplify\Security\Controllers\ConnectionRenewController;

    /**
     * GROUP - /connection
     * 
     * actions for controller device connections to applicaton
     */
    $this->group("/connection", function () {

        // POST: creating a new device connecton
        $this->post("[/]", ConnectionCreateController::class);

        // PUT: renewing an new connection
        $this->put("[/]", ConnectionRenewController::class);

        // PATHC: reinstating the connection
        $this->put("[/]", ConnectionReinstateController::class);

    });