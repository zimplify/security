<?php
    namespace Zimplify\Security;
    use Zimplify\Security\Interfaces\IAuthenticateableInterface;
    use Zimplify\Core\Instance;    
    use \DateTime;
    use \Exception;
    use \RuntimeException;

    /**
     * the Customer represent the END-USER of the application that is external to the organization
     * @package Zimplify\Security (code 02)
     * @type instnace (code 1)
     * @file Agent (code 01)
     */    
    abstract class Agent extends Instance implements IAuthenticateableInterface {

        const DEF_CLS_NAME = "Zimplify\\Security\\Agent";
        const FLD_FAILED_ATTEMPT = "failed";
        const FLD_LOCKED = "locked";
        const FLD_LOCKUP = "locking";
        const FLD_NETWORK = "network";
        const FLD_RESETTED = "resetted";
        const FLD_ROLES = "roles";
        const FLD_SECRET  = "secret";
        const FLD_UNLOCKED = "unlocked";

        /**
         * authenticating the user
         * @string string $secret the unlocking secret
         * @return bool
         */
        public function authenticate(string $secret) : bool {            
            $this->{self::FLD_ACCESSED} = new DateTime();
            if (($result = password_verify($secret, $this->{self::FLD_SECRET})) === false) {
                $this->{self::FLD_FAILED_ATTEMPT}++;
                if ($this->{self::FLD_FAILED_ATTEMPT} >= 3) $this->block();
            }
            $this->save();            
            return $result && ($this->{self::FLD_LOCKUP} === false);
        }

        /**
         * issue a block on agent access.
         * @return Agent
         */
        public function block() : self {
            $this->{self::FLD_LOCKUP} = true;
            $this->{self::FLD_LOCKED} = new DateTime();
            return $this;
        }

        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public abstract function generate(string $device, string $client) : string;

        /**
         * adding roles allowed for this user
         * @param array $roles the roles that is set to the user
         * @return IAuthenticateableInterface 
         */
        public function grant(array $roles) : IAuthenticateableInterface {
            foreach ($roles as $role) 
                if (!in_array($role, $this->roles)) 
                    array_push($this->roles, $role);
            return $this;
        }

        /**
         * check if a role is enabled for the agent
         * @param string $role the role in question
         * @return bool 
         */
        public function isPermitted(string $role) : bool {
            return in_array($role, $this->roles) || in_array("systemwide.permit", $this->roles);
        }

        /**
         * check if another agent is part of the agent's network
         * @param Agent $friend the agent we need to verify
         * @return bool
         */
        public function isNetworked(Agent $friend) : bool {
            return in_array($friend->id, $this->{self::FLD_NETWORK});
        }

        /**
         * adding a new friend in the network list
         * @param Agent $friend the party to add
         * @return Agent
         */
        public function join(Agent $friend) : self {
            array_push($friend->id, ($n = $this->network));
            $this->network = $n;
            return $this;
        }

        /**
         * the preparation steps of the instance during initialization
         * @return void
         */        
        protected function prepare() {
            $this->envoke("resetted", array($this,"notify"));
            $this->envoke("remove", array($this,"revoke"));            
        }
        
        /**
         * resetting the authentication secret for the agent
         * @param string $secret the new secret to hash
         * @return Agent
         */
        public function reset(string $secret) : self {
            $this->{self::FLD_SECRET} = $secret;
            $this->{self::FLD_RESETTED} = new DateTime();
            $this->unblock();
            $this->save();
            $this->invoke("resetted");
            return $this;
        }

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected abstract function revoke();

        /**
         * unblocking the access of an agent
         * @return Agent
         */
        public function unblock() : self {
            $this->{self::FLD_FAILED_ATTEMPT} = 0;
            $this->{self::FLD_LOCKUP} = false;
            $this->{self::FLD_UNLOCKED} = new DateTime();
            return $this;
        }

        /**
         * remove a user role from the user
         * @param string $role the role to remove
         * @return IAuthenticateableInterface
         */
        public function ungrant(string $role) : IAuthenticateableInterface {
            if (in_array($role, $this->roles)) 
                array_splice($this->roles, array_search($role, $this->roles), 1);
            return $this;
        }

        /**
         * remove the friend from the list in the network
         * @param string $friend the friend to remove from list
         * @return Agent
         */
        public function unjoin(string $friend) : self {
            if (($i = array_search($friend, $this->network)) > -1) {
                array_splice(($n = $this->network), $i, 1);
                $this->network = $n;                
            }
            return $this;
        }
    }