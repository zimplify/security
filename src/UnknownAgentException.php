<?php
    namespace Zimplify\Security;
    use Zimplify\Rest\Intetfaces\IResponseCodeInterface;
    use \Exception;

    /**
     * Exceptions during an agent is not found
     * @package Zimplify\Security (code 02)
     * @type exception (code 11)
     * @file UnknownAgentException (code 10)
     */
    class UnknownAgentException extends Exception implements IResponseCodeInterface {

        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , Throwable $previous = null) {
            parent::__construct($message, self::RES_NOT_FOUND, $previous);
        }        
    }