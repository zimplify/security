<?php
    namespace Zimplify\Security\Providers;
    use Zimplify\Core\Application;
    use Zimplify\Core\Provider;

    /**
     * The main provider for tokenization in CSB security
     * @package Zimplify\Security (code 2)
     * @instance Provider (code 03)
     * @type SecureTokenProvider (code: 01)
     */
    class SecureTokenProvider extends Provider {

        const CFG_KEY_HANDSKS = "security.keys.handshakes";
        const CFG_KEY_SESSION = "security.keys.session";
        const ERR_NO_HANDSHAKES =  401020301001;
        const ERR_NO_KEY = 500020301002;
        const KEY_SECKEY = "key";
        const PDR_CRYPTO = "core::crypto";

        /**
         * decoding a token from outside and get the source data
         * @param string $data the data coming in
         * @return array
         */
        public function decode(string $data) : array {
            $base = rawurldecode($data);
            $result = Application::request(self::PDR_CRYPTO, [self::KEY_SECKEY => $this->key()])->decrypt($base);
            return json_decode($result, true);
        }

        /**
         * encode a new token based on the data we have
         * @param array $data the data to encode
         * @return string 
         */
        public function encode(array $data) : string {
            $this->debug("ENCODE: ".json_encode($data));
            $result = Application::request(self::PDR_CRYPTO, [self::KEY_SECKEY => $this->key()])->encrypt(json_encode($data));
            return rawurlencode($result);
        }

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {}

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool { 
            return true; 
        }

        /**
         * get the encryption key
         * @return array
         */
        private function key() : array {
            $result = Application::env(self::CFG_KEY_SESSION);
            if (!$result) 
                throw new \RuntimeException("Unable to locate keys for operation", self::ERR_NO_KEY);
            return $result;
        }
    }