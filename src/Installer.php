<?php
    namespace Zimplify\Security;
    use Zimplify\Core\{Application, Query};
    use Zimplify\Core\Interfaces\IAuthorInterface;
    use Zimplify\Security\Agent;
    use \RuntimeException;

    /**
     * @package Zimplify\Security (code 02)
     * @type instance (code 1)
     * @file Installer (code 02)
     */
    final class Installer extends Agent implements IAuthorInterface {

        const CLS_ADMINISTRATOR = "security::administrator";
        const ERR_NOT_ALLOWED = 50002102001;
        const ERR_NOT_PERMITTED = 40302102002;

        /**
         * get documents under certain state
         * @param string $type the document state type
         * @return array
         */
        public function documents(string $type) : array {
            return [];
        }        
        
        /**
         * generating a new token for headers
         * @param string $device the device type when accessing
         * @param string $client the source address
         * @return string
         */
        public function generate(string $device, string $client) : string {
            throw new \RuntimeException("Not allowed for this object", self::ERR_NOT_ALLOWED);
        }

        /**
         * we need to complete all our preset here
         * @return void
         */
        protected function prepare() {   
            $users = Application::search([Query::SRF_TYPE => self::CLS_ADMINISTRATOR, Query::SRF_STATUS => true]);
            if (count($users) > 0)
                throw new RuntimeException("Installer is not applicable when application is already initialized.", self::ERR_NOT_PERMITTED);
        }

        /**
         * Installer is virtual so do not allow saving
         * @return void
         */
        protected function presave() {
            throw new RuntimeException("Installer should not exist in data schema", self::ERR_NOT_ALLOWED);
        }

        /**
         * handling the actions we need to do before deleting the account.
         * @return void
         */
        protected function revoke() {
        }                
    }