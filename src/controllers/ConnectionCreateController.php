<?php
    namespace Zimplify\Security\Controllers;
    use Zimplify\Core\{Applicaton, Controller, Query};
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Rest\Reply;
    use Zimplify\Rest\Intetfaces\IResponseCodeInterface;
    use Zimplify\Security\{UnauthorizedException, UnknownDeviceException};
    use Zimplify\Security\Interfaces\ITokenConsumerInterface;
    use Slim\Http\Request;
    use Firebase\JWT\JWT;
    use \DateTime;
    use \Exception;
    use \InvalidArgumentException;
    use \RuntimeException;

    /**
     * the ConnectionController controls device connection to the application
     * @package Zimplify\Security (code 2)
     * @type controller (code 04)
     * @file ConnectionCreateController (code 01)
     */
    class ConnectionCreateController extends Controller implements ITokenConsumerInterface, IResponseCodeInterface {

        const CFG_HANDSHAKES = "security.devices.handshakes";
        const CFG_AUTH_FREE = "security.devices.no-authentication";
        const CLS_AGENT = "Zimplify\\Security\\Agent";
        const DEF_HASH_FORM = "HS256";
        const FLD_TOKEN = "token";
        const PDR_SECURE_TOKEN = "core-scure::secure-token";
        const SRF_CONTACT_EMAIL = "contact.email";


        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $body = $req->getParsedBody();
            
            // make sure we received the token
            if (array_key_exists(self::FLD_TOKEN, $body)) {
                $data = null;
                $device = null;
                $agent = null;
                
                // 1. make sure we check all handshakes
                foreach (Application::env(self::CFG_HANDSHAKES) ?? [] as $assume => $hs) 
                    try {
                        $data = JWT::decode($body[self::FLD_TOKEN], $hs, [self::DEF_HASH_FORM]);

                        if (is_object($data))
                            if (property_exists($data, self::TKN_FLD_ADDRESS)) {
                                $device = $assume;
                                break;
                            } else 
                                throw new RuntimeException("Token expect the client details but none found.", self::RES_BAD_REQUEST);
                        else 
                            throw new RuntimeException("Token is invalid.", self::RES_BAD_REQUEST);
                    } catch (Exception $ex) {
                        // return nothing there...                        
                    }
                
                // 2. if no device, we bail
                if (!$device) 
                    throw new UnknownDeviceException("Cannot find matching device allowed on application.");

                // 3. check if device is exempted from authentication
                if (!in_array($device, Application::env(self::CFG_AUTH_FREE))) 

                    // 3a. make sure we have our fields
                    if (property_exists($data, self::TKN_FLD_AGENT) && property_exists($data, self::TKN_FLD_SECRET)) {
                        $agents = Application::search([self::SRF_CONTACT_EMAIL => $data->{self::TKN_FLD_AGENT}, Query::SRF_STATUS => true]);

                        // 3b. if we do have the instance, then do the authentication
                        if (count($agents) > 0) {
                            $agent = $agents[0];

                            // 3c. we finally need to complete the authenication run
                            if (!(property_exists($data, self::TKN_FLD_DEBUG_USER) && $data->{self::TKN_FLD_DEBUG_USER} === true)) 
                                if (!$agent->authenticate($data->{self::TKN_FLD_SECRET}))
                                    throw new UnauthorizedException("Challenge failed.");
                        } else 
                            throw new RuntimeException("Failed to identify the request agent.", 404);

                    } else 
                        throw new UnauthorizedException("Unexpected challange recevied");
                

                // 4. now we have to encode our data
                $encoder = Application::request(self::PDR_SECURE_TOKEN, []);
                $tkd = $encoder->encode([self::TKN_FLD_DEVICE => $device, self::TKN_FLD_ADDRESS => $data[self::FLD_TOKEN][self::TKN_FLD_ADDRESS]]);

                // 5. encoding our reply
                $result = new Reply();
                $headers = [Application::env(self::CFG_TOKEN_DEVICE) => $tkd];

                // 5a. if we have agent encode that in
                if ($agent && ClassUtils::is($agent, self::CLS_AGENT)) {
                    $headers[Application::env(self::CFG_TOKEN_USER)] = $agent->generate($device, $data[self::FLD_TOKEN][self::TKN_FLD_ADDRESS]);
                    $result->withStatus(208);
                }

                // 5b. finish the encode
                $result->withHeader($headers)->withJson(null);
                return $result;
            } else 
                throw new InvalidArgumentException("Expected token provided, but not found", 400);
        }
    }