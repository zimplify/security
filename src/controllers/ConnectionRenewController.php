<?php
    namespace Zimplify\Security\Controllers;
    use Zimplify\Core\{Applicaton, Controller};
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Rest\Reply;
    use Zimplify\Rest\Intetfaces\IResponseCodeInterface;
    use Zimplify\Security\Interfaces\ITokenConsumerInterface;;
    use Slim\Http\Request;
    use \InvalidArgumentException;
    use \RuntimeException;

    /**
     * the ConnectionCreateController allow user to renew their user token upon expiry
     * @package Zimplify\Security (code 2)
     * @type controller (code 04)
     * @file ConnectionRenewController (code 02)
     */
    class ConnectionRenewController extends Controller implements ITokenConsumerInterface, IResponseCodeInterface {

        const PDR_SECURE_TOKEN = "core-secure::secure-token";

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $tkd = $req->getHeader(Application::env(self::CFG_TOKEN_DEVICE));
            $tku = $req->getHeader(Application::env(self::CFG_TOKEN_USER));

            // 1. make sure we have our tokens
            if (count($tkd) > 0 && count($tku) > 0) {
                $tkd = $tkd[0];
                $tku = $tku[0];

                // 2. get the data out and check 
                if (is_array($data = Application::request(self::PDR_SECURE_TOKEN, [])->decode($tku))) 
                    if (array_key_exists(self::TKN_FLD_IDENTITY, $data) && array_key_exists(self::TKN_FLD_DEVICE, $data) &&
                        array_key_exists(self::TKN_FLD_EXPIRY, $data) && array_key_exists(self::TKN_FLD_ADDRESS, $data)) {

                        // 3. we need to make sure the agent is for real
                        $agent = Application::load($data[self::TKN_FLD_IDENTITY]);
                        
                        if ($agent && ClassUtils::is($agent, self::CLS_AGENT)) {
                            $tku = $agent->generate($data[self::TKN_FLD_DEVICE], $dasta[self::TKN_FLD_ADDRESS]);
                            $headers = [Applicaton::env(self::CFG_TOKEN_DEVICE) => $tkd, 
                                        Application::env(self::CFG_TOKEN_USER) => $agent->generate($data[self::TKN_FLD_DEVICE], $data[self::TKN_FLD_ADDRESS])];

                            $result = (new Reply())->withStatus(208)->withHeader($headers);
                            return $result;
                        } else 
                            throw new RuntimeException("Agent is not recognized.", self::RES_NOT_FOUND);                        
                    } else
                        throw new RuntimeException("Invalid agent token field detected.", self::RES_BAD_REQUEST);
                else 
                    throw new RuntimeException("Invalid agent token detected.", self::RES_BAD_REQUEST);
            } else 
                throw new RuntimeException("Expected both agent and device tokens but missing.", self::RES_BAD_REQUEST);
        }
    }