<?php
    namespace Zimplify\Security\Controllers;
    use Zimplify\Core\{Applicaton, Controller};
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Rest\Reply;
    use Zimplify\Rest\Intetfaces\IResponseCodeInterface;
    use Zimplify\Security\{UnknownAgentException};
    use Zimplify\Security\Interfaces\ITokenConsumerInterface;;
    use Slim\Http\Request;
    use \Exception;
    use \InvalidArgumentException;

    /**
     * the ConnectionReinstateController allow user to reclaim the token if they install the application
     * @package Zimplify\Security (code 2)
     * @type controller (code 04)
     * @file ConnectionReinstateController (code 03)
     */
    class ConnectionReinstateController extends Controller implements ITokenConsumerInterface, IResponseCodeInterface {

        const CFG_REINST_DEVICE = "security.device.reinstallable";
        const INF_REINSTALLABLE = "IReinstallableInterface";
        const PDR_SECURE_TOKEN = "core-secure::secure-token";
        const SRF_CONTACT_EMAIL = "contact.email";

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected function process(Request $req, array $args = []) : Reply {
            $tkd = $req->getHeader(Application::env(self::CFG_TOKEN_DEVICE));

            // 1. make sure we have device token
            if (count($tkd) > 0) {
                $tkd = $tkd[0];
                $body = $req->getParsedBody();

                // 2. make sure our token is good
                if (!(array_key_exists(self::TKN_FLD_DEVICE, $tkd) && array_key_exists(self::TKN_FLD_ADDRESS, $tkd))) 
                    throw new InvalidArgumentException("Token is corrupted.", self::RES_BAD_REQUEST);

                // 3. make sure we have our agent data
                if (array_key_exists(self::TLK_FLD_AGENT, $body)) {
                    $device = Application::request(self::PDR_SECURE_TOKEN, [])->decode($tkd);
                    $agent = Application::search([self::SRF_CONTACT_EMAIL => $body[self::TKN_FLD_AGENT]]);

                    // 4. make sure our agent is reinstallable
                    if (count($agent) > 0 && ClassUtils::is($agent[0], self::CLS_AGENT) && 
                        ClassUtils::isImplemented($agent[0], self::INF_REINSTALLABLE) &&
                        in_array($tkd[self::TKN_FLD_DEVICE], Application::env(self::CFG_REINST_DEVICE))) {
                            
                        // 5. now encoding our response
                        $headers = [Application::env(self::CFG_TOKEN_DEVICE) => $tkd,
                                    Application::env(self::CFG_TOKEN_DEVICE) => $agent->generate($tkd[self::TKN_FLD_DEVICE], $tkd[self::TKN_FLD_ADDRESS])];
                        $result = (new Reply())->withStatus(208)->withHeader($headers)->withJson(null);
                        return $result;
                    } else 
                        throw new UnknownAgentException("Request agent is not defined.");
                } else  
                    throw new InvalidArgumentException("Expecting agent key but not available", self::RES_BAD_REQUEST);
            } else 
                throw new InvalidArgumentException("Expecting device token", self::RES_BAD_REQUEST);
        }
    }