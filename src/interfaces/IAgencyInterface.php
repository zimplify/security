<?php
    namespace Zimplify\Security\Interfaces;

    /**
     * this interface allow use to identify agents in Controllers among other things
     * @package Zimplify\Security (code 02)
     * @type Interface (code 06)
     * @file IAuthenticateableInterface (code 05)
     */    
    interface IAgencyInterface {
        const ATTR_AGENT = "agent";
        const ATTR_DEVICE = "device";        
    }