<?php
    namespace Zimplify\Security\Interfaces;

    /**
     * this interface ensures the agent can reinstate tokens
     * @package Zimplify\Security (code 02)
     * @type interface (code 6)
     * @file IReinstallableInterface (code 04)
     */
    interface IReinstallableInterface {
    }