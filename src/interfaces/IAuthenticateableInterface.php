<?php
    namespace Zimplify\Core\Interfaces;

    /**
     * this interface allow us to enable identification items that has roles
     * @package Zimplify\Security (code 02)
     * @type Interface (code 06)
     * @file IAuthenticateableInterface (code 01)
     */
    interface IAuthenticateableInterface {

        const FLD_ROLES = "roles";

        /**
         * adding roles allowed for this user
         * @param array $roles the roles that is set to the user
         * @return IAuthenticateableInterface 
         */
        function grant(array $roles) : self;
        
        /**
         * remove a user role from the user
         * @param string $role the role to remove
         * @return IAuthenticateableInterface
         */
        function ungrant(string $role) : self;
        
    }