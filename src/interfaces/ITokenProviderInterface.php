<?php
    namespace Zimplify\Security\Interfaces;

    /**
     * this interface contains the common token variables to save retyping
     * @package Zimplify\Security (code 02)
     * @type interface (code 6)
     * @file ITokenProviderInterface (code 02)
     */
    interface ITokenProviderInterface {

        const HDF_OBJECT = "object";
        const HDF_DEVICE = "device";
        const HDF_ADDRESS = "address";
        const HDF_EXPIRY = "expiry";
        const PDR_SECURE_TOKEN = "core-secure::secure-token";

    }