<?php
    namespace Zimplify\Security\Interfaces;

    /**
     * this interface contains the common token variables to save retyping
     * @package Zimplify\Security (code 02)
     * @type interface (code 6)
     * @file ITokenConsumerInterface (code 03)
     */
    interface ITokenConsumerInterface {

        const CFG_TOKEN_DEVICE = "security.tokens.device";
        const CFG_TOKEN_USER = "security.tokens.agent";
        const TKN_FLD_ADDRESS = "address";
        const TKN_FLD_AGENT = "target";
        const TKN_FLD_DEBUG_USER = "debug";
        const TKN_FLD_DEVICE = "device";
        const TKN_FLD_IDENTITY = "object";
        const TKN_FLD_EXPIRY = "expiry";
        const TKN_FLD_SECRET = "secret";

    }