<?php
    namespace Zimplify\Security\Middlewares;
    use Zimplify\Core\Application;
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Rest\{Alert, Reply};
    use Zimplify\Rest\Interfaces\IResponseCodeInterface;
    use Zimplify\Security\{Agent, UnauthorizedException, UnknownAgentException};
    use Zimplify\Security\Interfaces\ITokenConsumerInterface;
    use Psr\Http\Message\ResponseInterface as Response;
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
    use \DateTime;
    use \InvalidArgumentException;

    /**
     * this middleware aims to check for agent in token and make sure it is provided
     * @package Zimplify\Security (code 02)
     * @type middleware (code 10)
     * @file AgentValidationMiddleware (code 01)
     */
    class AgentValidationMiddleware implements ITokenConsumerInterface, IResponseCodeInterface {

        const ATTR_AGENT = "agent";
        const ATTR_DEVICE = "device";
        const CLS_AGENT = "Zimplify\\Security\\Agent";
        const PDR_SECURE_TOKEN = "core-secure::secure-token";

        /**
         * the middleware validate the agent is presented and arm the agent onto the request
         * @param Request $req the incomng request
         * @param RequestHandler $handler the request next hop
         * @return Response
         */
        public function __invoke(Request $request, RequestHandler $handler): Response {
            $tkd = $req->getHeader(Application::env(self::CFG_TOKEN_DEVICE));
            $tku = $req->getHeader(Application::env(self::CFG_TOKEN_USER));

            // 1. make sure token are presented
            if (count($tku) > 0 && count($tkd) > 0) {
                $tkd = $tkd[0];
                $tku = $tku[0];

                // 2. loading out the agent data
                $data = Application::request(self::PDR_SECURE_TOKEN, [])->decode($tku);
                if (is_array($data) && array_key_exists(self::TKN_FLD_IDENTITY, $data)) {
                    $agent = Application::load($data[self::TKN_FLD_IDENTITY]);

                    // 3. we also need to validate the agent type if needed
                    if ($agent && ClassUtils::is($agent, self::CLS_AGENT) && $this->validate($agent)) {
                        $req = $req->withAttribute(self::ATTR_AGENT, $agent);
                        $req = $req->withAttribute(self::ATTR_DEVICE, $data[self::TKN_FLD_DEVICE]);
                        $result = $handler->handle($request);

                        // 4. handing the response
                        if (array_key_exists(self::TKN_FLD_EXPIRY, $data)) {
                            $expiry = $data[self::TKN_FLD_EXPIRY];
                            $remain = (new DateTime())->diff($expiry);

                            // 4a. if the time is less than 1 day, make sure we renew the token
                            if (($remain->invert == 0 && $remain->d == 1) || $remain->invert == 1) {
                                $device = Application::request(self::PDR_SECURE_TOKEN, [])->decode($tkd);

                                // 4a.a but make sure the token data is protected
                                if (array_key_exists(self::TKN_FLD_DEVICE, $device) && array_key_exists(self::TKN_FLD_ADDRESS, $device))
                                    $result = $result->withHeader(Application::env(self::CFG_TOKEN_DEVICE), 
                                        $agent->generate($device[self::TKN_FLD_DEVICE], $device[self::TKN_FLD_ADDRESS]));
                                else
                                    throw new InvalidArgumentException("Invalid device token.");
                            }
                        }

                        // 4b. adding the device token
                        $result = $result->withHeader(Application::env(self::CFG_TOKEN_DEVICE), $tkd);
                    } else 
                        $result = (new Alert(new UnknownAgentException("Agent is not available")))->flush();
                } else 
                    $result = (new Alert(new UnknownAgentException("Data is not presented for agent.")))->flush();
            } else 
                $result = (new Alert(new UnauthorizedException("No user token provided")))->flush();

            return $result;
        }

        /**
         * make sure the agnet is of the right privilege
         * @param Agent $agent the detected agent
         * @return bool
         */
        protected function validate(Agent $agent) : bool {
            return true;
        }
    }