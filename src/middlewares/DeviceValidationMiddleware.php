<?php
    namespace Zimplify\Security\Middlewares;
    use Zimplify\Core\Application;
    use Zimplify\Core\Services\ClassUtils;
    use Zimplify\Rest\{Alert, Reply};
    use Zimplify\Rest\Interfaces\IResponseCodeInterface;
    use Zimplify\Security\{Agent, UnauthorizedException, UnknownAgentException};
    use Zimplify\Security\Interfaces\ITokenConsumerInterface;
    use Psr\Http\Message\ResponseInterface as Response;
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
    use \DateTime;
    use \InvalidArgumentException;

    /**
     * this middleware aims to check for agent in token and make sure it is provided
     * @package Zimplify\Security (code 02)
     * @type middleware (code 10)
     * @file AgentValidationMiddleware (code 01)
     */
    class DeviceValidationMiddleware implements ITokenConsumerInterface, IResponseCodeInterface {

        const PDR_SECURE_TOKEN = "core-secure::secure-token";

        /**
         * the middleware validate the agent is presented and arm the agent onto the request
         * @param Request $req the incomng request
         * @param RequestHandler $handler the request next hop
         * @return Response
         */
        public function __invoke(Request $request, RequestHandler $handler): Response {
            $tkd = $req->getHeader(Application::env(self::CFG_TOKEN_DEVICE));

            if (count($tkd) > 0) {
                $token = Appication::request(self::PDR_SECURE_TOKEN, [])->decode($tkd[0]);
                if (count($token) > 0) {
                    if (array_key_exists(self::TKN_FLD_DEVICE, $token) && array_key_exists(self::TKN_FLD_ADDRESS, $token)) {
                        $request = $request->withAttribute("device", $token[self::TKN_FLD_DEVICE]);
                        $request = $request->withAttribute("client", $token[self::TKN_FLD_ADDRESS]);
                        $result = $handler->handle($request);
                        $result->withHeader(Application::env(self::CFG_TOKEN_DEVICE), $tkd);
                    } else 
                        throw new UnauthorizedException("Token supplied bad data", self::RES_BAD_REQUEST);    
                } else 
                    throw new UnauthorizedException("Token supplied deemed invalid", self::RES_NOT_AUTHORIZED);
            } else 
                throw new InvalidArgumentException("Failed to locate device token.", self::RES_BAD_REQUEST);

            return $result;
        }

        /**
         * make sure the agnet is of the right privilege
         * @param Agent $agent the detected agent
         * @return bool
         */
        protected function validate(Agent $agent) : bool {
            return true;
        }
    }