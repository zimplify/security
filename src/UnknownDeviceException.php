<?php
    namespace Zimplify\Security;
    use Zimplify\Rest\Intetfaces\IResponseCodeInterface;
    use \Exception;

    /**
     * Exceptions during unknowned device attempt
     * @package Zimplify\Security (code 02)
     * @type exception (code 11)
     * @file UnknownDeviceException (code 9)
     */
    class UnknownDeviceException extends Exception implements IResponseCodeInterface {
        
        /**
         * starting up the instance
         * @param string $message (optional) the message to let the user know what happened
         * @param Throwable $previous (optional) the exception triggered this one.
         * @return void
         */
        function __construct (string $message = "" , Throwable $previous = null) {
            parent::__construct($message, self::RES_INTERNAL_ERROR, $previous);
        }
    }