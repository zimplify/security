<?xml version="1.0" encoding="UTF-8"?>
<data name="authentication">
    <data name="secret" type="hash" required="true" />
    <data name="renewed" type="date" />
    <data name="history" type="array" split="," />
</data>