<?xml version="1.0" encoding="UTF-8"?>
<data name="contact">
    <data name="name">
        <data name="first" type="text" />
        <data name="last" type="text" />
    </data>
    <data name="email" type="text" />
    <data name="mobile" type="text" />
    <data name="dob">
        <data name="year" type="number" />
        <data name="month" type="number" />
        <data name="day" type="number" />
    </data>
    <data name="devices" type="array" delimiter="," />
</data>